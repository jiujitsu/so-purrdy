from bs4 import BeautifulSoup
from flask import Flask, render_template, request

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'GET':
        return render_template('index.html', pretty=None)
    elif request.method == 'POST':
        ugly = request.form['ugly_html']
        soup = BeautifulSoup(ugly, 'html5lib')

        return render_template('pretty.html', pretty=soup.prettify())

if __name__ == '__main__':
    # TODO: add command line arguments to define runmode (dev/prod) which
    # will enable/disable debug mode
    app.run(debug=True)
